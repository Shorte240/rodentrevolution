﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HobScript : MonoBehaviour
{
    public float jumpHeight = 0.5f;
    
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            collision.gameObject.GetComponent<Rigidbody>().AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Impulse);
        }
    }
}
