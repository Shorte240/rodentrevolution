﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseController : MonoBehaviour
{
    public GameObject ball;

    public float offsetY = 0.1f;

    // Update is called once per frame
    void Update()
    {
        transform.position = ball.transform.position - new Vector3(0.0f, offsetY, 0.0f);
        transform.rotation = Camera.main.transform.rotation;
    }
}
