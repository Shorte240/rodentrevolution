﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UIScript : MonoBehaviour
{
    // Objects to show in the Pause Menu
    public GameObject[] pauseObjects;

    // Use this for initialization
    void Start()
    {
        // Set default pause values
        Time.timeScale = 1;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        HidePaused();
    }

    // Hide all objects tagged with "ShowOnPause"
    public void HidePaused()
    {
        foreach (GameObject obj in pauseObjects)
        {
            obj.SetActive(false);
        }
    }

    // Show all objects tagged with "ShowOnPause"
    public void ShowPaused()
    {
        foreach (GameObject obj in pauseObjects)
        {
            obj.SetActive(true);
        }
    }

    // Restart the current level
    public void RestartLevel(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    // Go to the main menu
    public void GoToMenu(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    // Control the pause option
    public void PauseControl()
    {
        if (Time.timeScale == 1)
        {
            Time.timeScale = 0;
            ShowPaused();
        }
        else if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
            HidePaused();
        }
    }
}
