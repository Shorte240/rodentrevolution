﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public GameObject player;

    private Vector3 offset;

    public float rotationSpeed = 20.0f;

    float turnAngle = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        offset = transform.position - player.transform.position;
    }

    void Update()
    {
        turnAngle += Input.GetAxis("RightStickHorizontal") * rotationSpeed * Time.deltaTime;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (player != null)
        {
            transform.position = player.transform.position + offset;
            transform.RotateAround(player.transform.position, Vector3.up, turnAngle);
            transform.LookAt(player.transform); 
        }
    }
}
