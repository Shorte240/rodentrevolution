﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyFlaskScript : MonoBehaviour
{
    public GameObject stopper;

    public GameObject liquid;

    public GameObject[] destroyedObject;

    public GameObject splash;

    public AudioClip[] sounds;

    private AudioSource source;

    public float offsetY = 0.3f;

    public float waitTime = 0.01f;

    public float destroyedValue = 200.0f;

    public float timeToAdd = 2.0f;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            PlayRandomSound();

            GetComponent<MeshRenderer>().enabled = false;

            GetComponent<MeshCollider>().enabled = false;

            StartCoroutine(Wait());

            foreach (GameObject obj in destroyedObject)
            {
                Instantiate(obj, transform.position + new Vector3(0.0f, offsetY, 0.0f), transform.rotation);
            }

            
            stopper.GetComponent<Rigidbody>().isKinematic = false;

            liquid.SetActive(false);

            Instantiate(splash, splash.transform.position, splash.transform.rotation);

            collision.gameObject.GetComponent<BallController>().time += timeToAdd;
        }
    }

    void PlayRandomSound()
    {
        source.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }
}
