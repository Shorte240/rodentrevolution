﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    private void Start()
    {
        if (Time.timeScale == 0)
        {
            Time.timeScale = 1;
        }
    }

    // Load specified scene
    public void GoToScene(string SceneName)
    {
        SceneManager.LoadScene(SceneName);
    }

    // Close application
    public void QuitGame()
    {
        Application.Quit();
    }
}
