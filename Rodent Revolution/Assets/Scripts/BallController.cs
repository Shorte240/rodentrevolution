﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BallController : MonoBehaviour
{
    public GameObject eS;

    public Animator anim;

    public float moveSpeed = 200.0f;

    public float jumpHeight = 200.0f;

    public float waitTime = 0.5f;

    Transform cheat;

    public GameObject trailDust;

    public GameObject landDust;

    public AudioClip landingSound;

    public AudioClip rollingSound;

    public AudioClip[] squeaks;

    public AudioSource source;

    public AudioSource rollingSource;

    public Text totalMice;
    public Text miceFreed;
    public Text totalDamage;
    public Text timeRemaining;

    public float freedMice = 0.0f;
    public float totalMiceCounter;
    public float time = 120.0f;
    public string finalScene = "FailScene";
    public float totDam;
    private Rigidbody rb;

    // Start is called before the first frame update
    void Start()
    {
        totDam = 0.0f;
        cheat = new GameObject().transform;
        source = GetComponent<AudioSource>();
        totalMiceCounter = GameObject.FindGameObjectsWithTag("Cage").Length;
        totalMice.text = ("BUDDIES FREE:    /" + totalMiceCounter);
        totalDamage.text = "" + totDam;
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        MoveBall();

        if (!trailDust.activeSelf)
        {
            StartCoroutine(Wait());
        }

        if (Input.GetAxis("Options") != 0 && !eS.GetComponent<UIScript>().pauseObjects[0].activeSelf)
        {
            eS.GetComponent<UIScript>().PauseControl();
        }

        UpdateText();

        CheckWinCondition();
    }

    void MoveBall()
    {

        if (Input.GetAxis("LeftStickHorizontal") != 0 || Input.GetAxis("LeftStickVertical") != 0)
        {
            float hor = Input.GetAxis("LeftStickHorizontal");
            float ver = Input.GetAxis("LeftStickVertical");

            Vector3 dir = new Vector3(hor, 0, ver);
            Vector3 rot = new Vector3(ver, 0, -hor);

            cheat.position = transform.position;
            cheat.rotation = Camera.main.transform.rotation;
            Vector3 lookAt = cheat.position + cheat.forward;
            lookAt.y = transform.position.y;
            cheat.transform.LookAt(lookAt);

            rb.AddTorque(cheat.TransformDirection(rot) * moveSpeed * Time.deltaTime);
            rb.AddForce(cheat.TransformDirection(dir) * moveSpeed * Time.deltaTime);
        }
        
        if (Input.GetAxis("Cross") != 0 && rb.velocity.y <= 0.15f && rb.velocity.y >= 0.0f && Time.timeScale != 0)
        {
            rb.AddForce(new Vector3(0, jumpHeight, 0), ForceMode.Impulse);
            anim.SetBool("Jump", true);
        }

        if (rb.velocity.magnitude >= 0.2 && !rollingSource.isPlaying && rb.velocity.y == 0)
        {
            rollingSource.clip = rollingSound;
            rollingSource.Play();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ground")
        {
            source.PlayOneShot(landingSound);
            landDust.SetActive(true);
            trailDust.SetActive(false);
            anim.SetBool("Jump", false);
            rb.velocity.Set(rb.velocity.x, 0, rb.velocity.z);
        }
        else if (collision.gameObject.name == "Flask1" || collision.gameObject.name == "Beaker1" || collision.gameObject.name == "TestTube1" || collision.gameObject.name == "Bowl1")
        {
            if (collision.gameObject.GetComponent<Destructible>())
            {
                totDam += collision.gameObject.GetComponent<Destructible>().destroyedValue;
            }
            else if (collision.gameObject.GetComponent<DestroyFlaskScript>())
            {
                totDam += collision.gameObject.GetComponent<DestroyFlaskScript>().destroyedValue;
            }
            totalDamage.text = "" + totDam;
            source.PlayOneShot(squeaks[Random.Range(0, squeaks.Length)]);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Table" || other.gameObject.tag == "Shelf" || other.gameObject.tag == "TopCage")
        {
            source.PlayOneShot(landingSound);
            landDust.SetActive(true);
            trailDust.SetActive(false);
            anim.SetBool("Jump", false);
            rb.velocity.Set(rb.velocity.x, 0, rb.velocity.z);
        }
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(waitTime);
        landDust.SetActive(false);
        trailDust.SetActive(true);
    }

    // Go to the main menu
    public void LoadScene(string levelName)
    {
        SceneManager.LoadScene(levelName);
    }

    void UpdateText()
    {
        if (miceFreed != null)
        {
            miceFreed.text = ("" + freedMice + "  ");
        }

        if (timeRemaining != null)
        {
            if (time > 0)
            {
                time -= Time.deltaTime;

                timeRemaining.text = (time.ToString("F2") + "s");
            }
            else
            {
                LoadScene(finalScene);
            }
        }
    }

    void CheckWinCondition()
    {
        if (freedMice == totalMiceCounter)
        {
            if (SceneManager.GetActiveScene().name != "FailScene")
            {
                LoadScene(finalScene); 
            }
            return;
        }
    }
}
