﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VentController : MonoBehaviour
{
    public Transform[] vents;

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            collision.gameObject.transform.position = vents[Random.Range(0, vents.Length)].position;
        }
    }
}
