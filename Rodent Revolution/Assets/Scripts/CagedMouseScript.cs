﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CagedMouseScript : MonoBehaviour
{
    public AudioSource crying;
    public AudioSource cheering;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (cheering.enabled)
        {
            crying.enabled = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Ball")
        {
            crying.enabled = true;
            crying.Play();
        }
    }
}
