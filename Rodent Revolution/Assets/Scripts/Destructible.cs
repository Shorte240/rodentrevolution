﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destructible : MonoBehaviour
{
    public GameObject[] destroyedObject;

    public AudioClip[] sounds;

    private AudioSource source;

    public float offsetY = 0.3f;

    public float waitTime = 0.01f;

    public float destroyedValue = 100.0f;

    public float timeToAdd = 2.0f;

    private void Awake()
    {
        source = GetComponent<AudioSource>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball")
        {
            PlayRandomSound();

            GetComponent<MeshRenderer>().enabled = false;

            if (GetComponent<MeshCollider>())
            {
                GetComponent<MeshCollider>().enabled = false;
            }
            else if (GetComponent<BoxCollider>())
            {
                GetComponent<BoxCollider>().enabled = false;
            }

            StartCoroutine(Wait());

            foreach (GameObject obj in destroyedObject)
            {
                Instantiate(obj, transform.position + new Vector3(0.0f, offsetY, 0.0f), transform.rotation);
            }

            collision.gameObject.GetComponent<BallController>().time += timeToAdd;
        }
    }

    void PlayRandomSound()
    {
        source.PlayOneShot(sounds[Random.Range(0, sounds.Length)]);
    }

    IEnumerator Wait()
    {
        yield return new WaitForSeconds(waitTime);
        Destroy(gameObject);
    }
}
