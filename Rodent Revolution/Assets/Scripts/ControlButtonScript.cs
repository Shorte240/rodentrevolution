﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlButtonScript : MonoBehaviour
{
    public GameObject eS;
    public GameObject backButton;
    public GameObject playButton;
    public GameObject controls;

    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Player"))
        {
            Destroy(GameObject.FindGameObjectWithTag("Player"));
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (controls.activeSelf)
        {
            eS.GetComponent<EventSystem>().SetSelectedGameObject(backButton);
        }
    }

    public void SetPlayButton()
    {
        controls.SetActive(false);
        eS.GetComponent<EventSystem>().SetSelectedGameObject(playButton);
    }
}
