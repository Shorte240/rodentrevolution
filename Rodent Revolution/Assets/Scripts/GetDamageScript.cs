﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetDamageScript : MonoBehaviour
{
    public Text damage;
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Ball"))
        {
            damage.text = (GameObject.FindGameObjectWithTag("Ball").GetComponent<BallController>().totDam.ToString());
        }
    }
}
