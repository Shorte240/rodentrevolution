﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GetMiceScript : MonoBehaviour
{
    public Text mice;
    public Text totalMice;
    // Start is called before the first frame update
    void Start()
    {
        if (GameObject.FindGameObjectWithTag("Ball"))
        {
            mice.text = (GameObject.FindGameObjectWithTag("Ball").GetComponent<BallController>().freedMice.ToString());
            totalMice.text = ("Buddies Free:    /" + GameObject.FindGameObjectWithTag("Ball").GetComponent<BallController>().totalMiceCounter.ToString());
        }
    }
}
