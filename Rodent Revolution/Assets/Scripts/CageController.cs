﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CageController : MonoBehaviour
{
    public AudioSource cheering;

    public Animator anim;
    public Animator mouseAnim;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Ball" && gameObject.tag == "Cage")
        {
            cheering.enabled = true;
            cheering.Play();
            anim.enabled = true;
            mouseAnim.SetBool("Free", true);
            collision.gameObject.GetComponent<BallController>().freedMice += 1;
            gameObject.tag = "OpenCage";
        }
    }
}
